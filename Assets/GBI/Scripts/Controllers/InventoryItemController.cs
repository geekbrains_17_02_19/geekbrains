namespace Geekbrains
{
    /// <summary>
    /// Класс контроллера вещи для инвентаря
    /// </summary>
    /// <see cref="Geekbrains.InventoryModel"/>
    /// <see cref="BaseController{T}"/>
    public abstract class InventoryItemController : BaseController<InventoryItemModel> { }
}