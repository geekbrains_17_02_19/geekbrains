using System;
using UnityEngine;

namespace Geekbrains
{
    public partial class DIContainer
    {
        /// <summary>
        /// Метод для создания и инъекции нового объекта
        /// </summary>
        /// <typeparam name="T">тип объекта</typeparam>
        /// <returns>Инъектированный объект</returns>
        public T Instantiate<T>() 
            where T : new()
        {
            var instantiate = new T();
            Inject(instantiate);
            return instantiate;
        }

        /// <summary>
        /// Метод для создания и инъекции нового объекта с параметрами
        /// </summary>
        /// <param name="args">параметры</param>
        /// <typeparam name="T">тип объекта</typeparam>
        /// <returns>Инъектированный объект</returns>
        public T Instantiate<T>(params object[] args)
        {
            var instantiate = (T) Activator.CreateInstance(typeof(T), args);
            Inject(instantiate);
            return instantiate;
        }

        /// <summary>
        /// Метод для создания и инъекции компонента в новом объекте
        /// </summary>
        /// <typeparam name="T">тип компонента</typeparam>
        /// <returns>Инъектированный компонент</returns>
        public T InstantiateComponent<T>() 
            where T : Component
        {
            var gameObject = new GameObject();
            var component = gameObject.AddComponent<T>();
            Inject(component);
            return component;
        }
        
        /// <summary>
        /// Метод для создания и инъекции компонента в существующий объект
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <typeparam name="T">тип компонента</typeparam>
        /// <returns>Инъектированный компонент</returns>
        public T InstantiateComponent<T>(GameObject obj) 
            where T : Component
        {
            var component = obj.AddComponent<T>();
            Inject(component);
            return component;
        }
    }
}