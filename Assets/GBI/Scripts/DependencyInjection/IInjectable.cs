namespace Geekbrains
{
    public interface IInjectable
    {
        /// <summary>
        /// Метод, вызываемый по окончанию инъекции
        /// </summary>
        void OnInject();
    }
}