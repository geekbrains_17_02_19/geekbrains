namespace Geekbrains
{
    /// <summary>
    /// Перечисление языков игры <br/>
    /// Должно быть полное именование языка без сокращений
    /// </summary>
    public enum GameLanguageEnum
    {
        ENGLISH,
        RUSSIAN
    }
}