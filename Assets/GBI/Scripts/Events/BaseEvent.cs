﻿namespace Geekbrains
{
    /// <summary>
    /// Базовый абстрактный класс события <br/>
    /// Содержит в себе данные, которые передеаются слушателю параметорм 
    /// </summary>
    /// <seealso cref="Geekbrains.IEventDispatcher"/>
    /// <br/>
    /// <seealso cref="IEventListener{T}"/>
    public abstract class BaseEvent
    {
        
    }
}