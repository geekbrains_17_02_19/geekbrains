namespace Geekbrains {
    /// <summary>
    /// Модель вещей инвентаря
    /// </summary>
    /// <see cref="BaseModel"/>
    public abstract class InventoryItemModel : BaseModel {}
}