namespace Geekbrains
{
    /// <summary>
    /// Модель паузы
    /// </summary>
    /// <see cref="BaseModel"/>
    public class PauseModel : BaseModel
    {
        /// <summary>
        /// Флаг текущего состояния паузы
        /// </summary>
        public bool IsPaused;
    }
}