﻿namespace Geekbrains
{
    /// <summary>
    /// Перечисления направления изменения параметра
    /// </summary>
    internal enum ChangerEnum
    {
        Increase,
        Decrease
    }
}