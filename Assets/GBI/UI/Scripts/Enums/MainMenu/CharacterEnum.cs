﻿namespace Geekbrains
{
    /// <summary>
    /// Перечисление возможных типов главного персонажа
    /// </summary>
    internal enum CharacterEnum
    {
        ArcherMen,
        WarlockMen,
        WariorMen,
        ArcherWomen,
        WarlockWomen,
        WariorWomen
    }
}