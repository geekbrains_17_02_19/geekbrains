﻿namespace Geekbrains
{
    /// <summary>
    /// Перечисление качества для параметра
    /// </summary>
    internal enum QualityEnum
    {
        Low,
        Medium,
        High
    }
}
