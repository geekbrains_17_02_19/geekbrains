﻿namespace Geekbrains
{
    /// <summary>
    /// Перечисление разрешений экрана
    /// </summary>
    internal enum ResolutionsEnum
    {
        SVGA,
        XGA,
        WXGA,
        HD,
        FHD,
        WUXGA,
        UHD
    }
}
