﻿namespace Geekbrains
{
    /// <summary>
    /// Интерфейс контроллера меню
    /// </summary>
    internal interface IMenuController
    {
        /// <summary>
        /// Метод отображения меню
        /// </summary>
        void Show();

        /// <summary>
        /// Метод скрытия меню
        /// </summary>
        void Hide();
    }
}